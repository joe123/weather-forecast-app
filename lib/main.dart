import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:graduationproject/WeatherForecastResponse.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';

import 'Page2.dart';

void main() => runApp(MaterialApp(
  home: MyApp(),
));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<ListItem> _dropdownItems = [
    ListItem(1, "Cairo"),
    ListItem(2, "London"),
    ListItem(3, "Berlin"),
    ListItem(4, "Washington"),
    ListItem(5, "Moscow"),
    ListItem(5, "Kuwait"),

  ];
  List<DropdownMenuItem<ListItem>> _dropdownMenuItems;
  ListItem _itemSelected;
String selectedCity="";
List<Days> Weather;
  void initState() {
    super.initState();
     Weather = List<Days>();
     // Days LS1 = new Days(25, "Monday");
     // Days LS2= new Days (20,"Tuesday");
     // Weather.add(LS1);
     // Weather.add(LS2);
    _dropdownMenuItems = buildDropDownMenuItems(_dropdownItems);
    _itemSelected = _dropdownMenuItems[1].value;

  }

  List<DropdownMenuItem<ListItem>> buildDropDownMenuItems(List listItems) {
    List<DropdownMenuItem<ListItem>> items = List();
    for (ListItem listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items;
  }
  Future<WeatherForecastResponse> fetchWeather(String cityname) async {
    final http.Response response=await http.get(
      'https://api.openweathermap.org/data/2.5/forecast?q=$cityname&appid=1edc9b8317c194deb1e3969f23831694&units=metric',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return WeatherForecastResponse.fromJson(jsonDecode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load Weather');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Weather forecast"),
      ),
      body: Column(
        children: <Widget>[
          RaisedButton(child:Text("Get current location weather "),

          onPressed:() {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Page2()),
            );
          },),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  color: Colors.greenAccent,
                  border: Border.all()),
              child: DropdownButtonHideUnderline(
                child: DropdownButton(
                  hint: Text('Select City'),
                    // value: _itemSelected,
                    items: _dropdownMenuItems,
                    onChanged: (value) async {
                      setState(() {
                        _itemSelected = value;
                      });
                      selectedCity= 'Selected City : ${_itemSelected.name}';
                      final ProgressDialog pr = ProgressDialog(context);
                      pr.show();
                      var response = await fetchWeather(_itemSelected.name);
pr.hide();
                      setState(() {
                        for(int i = 0; i<response.list.length ;i++)
                        {

                          var date = DateTime.fromMillisecondsSinceEpoch(response.list[i].dt * 1000);
                          String hour=date.hour.toString().length==1?'0'+date.hour.toString():date.hour.toString();
                          String minute='0'+date.minute.toString();

                          String theDate='${date.day}-${date.month}-${date.year}  $hour:$minute';
                          Days Day = new Days (response.list[i].main.temp,theDate);
                          Weather.add(Day);
                        }
                      });
                      pr.hide();
                    }),
              ),
            ),
          ),
          Text(selectedCity),
          Expanded(child:ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: Weather.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text('${Weather[index].date}'),
                trailing: Text('${Weather[index].temp} C'),
              );
            },
          ) ,),

        ],
      ),
    );
  }
}

class Days {
  num temp;
  var date;

  Days(this.temp, this.date);

  }


class ListItem {
  int value;
  String name;

  ListItem(this.value, this.name);
}

