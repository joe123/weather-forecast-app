import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'WeatherLocationResponse.dart';
import 'package:progress_dialog/progress_dialog.dart';
ProgressDialog pr;
void main() => runApp(Page2());

class Page2 extends StatefulWidget {
  @override
  _MyAppState1 createState() => _MyAppState1();
}


class _MyAppState1 extends State<Page2> {
String temp='Please press button to get weather for current location';

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(message: 'Please wait');
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter Maps Demo'),
          backgroundColor: Colors.green,
        ),
        body: Stack(
          children: <Widget>[
            Center(
                child:Text(
                 '$temp',
                  style: TextStyle(
                      fontSize: 35,
                      color: Colors.purple,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.italic,
                      letterSpacing: 8,
                      wordSpacing: 20,
                      backgroundColor: Colors.yellow,
                      shadows: [
                        Shadow(color: Colors.blueAccent, offset: Offset(2,1), blurRadius:10)
                      ]
                  ),
                )
            ),

            Padding(
              padding: const EdgeInsets.all(14.0),
              child: Align(
                alignment: Alignment.topCenter,
                child: FloatingActionButton(
                  onPressed: ()async{
                    final ProgressDialog pr = ProgressDialog(context);
                    pr.show();
                    await getWeatherLocation();
                    pr.hide();
                  },
                  materialTapTargetSize: MaterialTapTargetSize.padded,
                  backgroundColor: Colors.green,
                  child: const Icon(Icons.map, size: 30.0),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
   void  getWeatherLocation()async{

    Location location = new Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    //For normal dialog

    _locationData = await location.getLocation();
   var result=await fetchWeather(_locationData.latitude,_locationData.longitude);
   setState(() {
     temp='Current weather for current location is '+result.current.temp.toString()+' C';
   });
    debugPrint('${_locationData.latitude}');
  }
  Future<WeatherLocationResponse> fetchWeather(double lat,double long) async {

    final http.Response response=await http.get(
      'https://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$long&appid=1edc9b8317c194deb1e3969f23831694&units=metric',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      var weather;
      return WeatherLocationResponse.fromJson(jsonDecode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }
}
